package tw.monsta.spse;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * class description of ApplicationUsingSpringApplicationBuilder
 *
 * @author johnson
 */
@SpringBootApplication
public class ApplicationUsingSpringApplicationBuilder {

    public static void main(String[] args) {
        new SpringApplicationBuilder()
//                .showBanner(false)
                .sources(ApplicationUsingSpringApplicationBuilder.class)
                .run(args);
    }
}
