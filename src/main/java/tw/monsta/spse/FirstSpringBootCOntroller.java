package tw.monsta.spse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Index description
 *
 * @author johnson
 * @since 2015-05-07
 */
@Controller
//@SpringBootApplication
public class FirstSpringBootController {

    @RequestMapping("/")
    public String home() {
        return "greeting";
    }

//    public static void main(String[] args) {
//        SpringApplication.run(FirstSpringBootController.class, args);
//    }
}
