package tw.monsta.spse.commandLineRunner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * class description of SampleCommandLineRUnner
 *
 * @author johnson
 */
@Order(value = 100)
@Component
public class FirstSampleCommandLineRunnerBean implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("this is a command line runner~~~~~~");
    }
}
