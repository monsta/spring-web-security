package tw.monsta.spse.commandLineRunner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * class description of RandomNumberFromPropertiesUsungCLRPrint
 *
 * @author johnson
 */
@Component
public class RandomNumberFromPropertiesUsungCLRPrint implements CommandLineRunner {

    @Value("${my.secret}")
    private String randomString;

    @Value("${my.int}")
    private int randomInt;

    @Value("${my.int.max.10}")
    private int max10Int;

    @Value("${my.int.range}")
    private int intWithRange;

    @Value("${my.long}")
    private long bignumber;

    @Override
    public void run(String... strings) throws Exception {
        System.out.println("my.secret: " + randomString);
        System.out.println("my.int: " + randomInt);
        System.out.println("my.int.max.10: " + max10Int);
        System.out.println("my.int.range: " + intWithRange);
        System.out.println("my.long: " + bignumber);
    }
}
