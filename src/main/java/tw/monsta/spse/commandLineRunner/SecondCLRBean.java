package tw.monsta.spse.commandLineRunner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * class description of SecondCLRBean
 *
 * @author johnson
 */
@Order(value = 200)
@Component
public class SecondCLRBean implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("command line runner NO.2~~~~~~~");
    }
}
